//
//  SomeFramework.h
//  SomeFramework
//
//  Created by Matthias Keiser on 15/06/2021.
//

#import <Foundation/Foundation.h>

//! Project version number for SomeFramework.
FOUNDATION_EXPORT double SomeFrameworkVersionNumber;

//! Project version string for SomeFramework.
FOUNDATION_EXPORT const unsigned char SomeFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SomeFramework/PublicHeader.h>


