//
//  XCPerformanceTestsTests.swift
//  XCPerformanceTestsTests
//
//  Created by Matthias Keiser on 15/06/2021.
//

import XCTest
@testable import XCPerformanceTests

class XCPerformanceTestsTests: XCTestCase {

    func testPerformanceExample1() throws {
        // This is an example of a performance test case.
        self.measure {
            _ = (0..<100).reduce(into:0) { $0 += $1}
        }
    }

    func testPerformanceExample2() throws {
        // This is an example of a performance test case.
        self.measure {
            _ = (0..<100).reduce(into:0) { $0 += $1}
        }
    }
}
