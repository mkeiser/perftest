//
//  XCPerformanceTestsTests
//
//  Created by Matthias Keiser on 15/06/2021.
//

import XCTest

class AnotherTest: XCTestCase {

    func testPerformanceExampleA() throws {
        // This is an example of a performance test case.
        self.measure {
            _ = (0..<100).reduce(into:0) { $0 += $1}
        }
    }

    func testPerformanceExampleB() throws {
        // This is an example of a performance test case.
        self.measure {
            _ = (0..<100).reduce(into:0) { $0 += $1}
        }
    }
}
